CONSIDERATIONS:  
*The same word in uppercase and lowercase will be considered as 2 different words.  
*The matrix generation and word list are out of scope and are defined in a fixed form. To modify the scenario, the GetMatrix and GetWordStream methods of the ChallengeHelper class must be modified.  
*There is no data repository. Both the matrix and the word list are in memory.  
*The search is performed once per execution.  
*In case of an execution error in the WordFinder class, a   controlled exception   is thrown.  


ABOUT THE RESOLUTION	
The constructor of the WordFinder class receives the original matrix that is stored in a private variable. The original matrix is also inverted by scrolling through columns from top to bottom, thus obtaining a new matrix to perform vertical searches. This new list is also stored in a private variable.  
In the Find method, a GroupBy is applied on the word stream and with the result a Dictionary<string,long> is generated where the Key will be the GroupBy's Key (the word) and the Value the amount of elements in each subgroup. In this way, it is known how many occurences there are of each word in the word stream.  
Each element in the Dictionary's Keys is verified if it appears in the horizontal or vertical matrix stored in the constructor. If the element does not appear in either matrix, it is removed from the Dictionary.  
Once all the words of the Dictionary have been processed, only the ones that appear in the matrix will remain and it will only be necessary to sort them in descending order applying an OrderByDescending on the Dictionary Value and keeping the first 10 using a Take(10). From then on, it is necessary to apply a Select to only keep the Keys of the Dictionary, obtaining the final list as a result.  


EXAMPLE  
For this example the words from the worrd stream that appears in the matrix are:
sun
winter  
cold  
cloud  
stone  
never  
storm  
table  
cat  
word  
country  
city  

And the top 10 are:
sun
winter  
cold  
cloud  
stone  
never  
storm  
table  
cat  
word  
