﻿using System;
using System.Collections.Generic;

namespace QuBeyond.Challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var matrix = ChallengeHelper.GetMatrix();
            var wordstream = ChallengeHelper.GetWordStream();
            var wordFinder = new WordFinder(matrix);


            var tops = wordFinder.Find(wordstream);
            foreach (var word in tops)
            {
                Console.WriteLine(word);
            }
            Console.ReadKey();

        }

        #region Auxiliary Methods

        #endregion
    }
}
