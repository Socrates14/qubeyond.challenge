﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuBeyond.Challenge
{
    public class WordFinder
    {

        const int MATRIX_SIZE = 64;
        private IEnumerable<string> _horizontalMatrix;
        private IEnumerable<string> _verticalMatrix;
        public WordFinder(IEnumerable<string> matrix)
        {

            try
            {
                _horizontalMatrix = matrix.ToList();
                _verticalMatrix = ConvertToVerticalMatrix(matrix.ToList());
            }
            catch (Exception e)
            {

                throw new Exception("Execution Exception", e);
            }
        }

        private IEnumerable<string> ConvertToVerticalMatrix(IEnumerable<string> matrix)
        {
            var list = new List<string>();
            for (int i = 0; i < MATRIX_SIZE; i++)
            {
                string word = "";
                for (int j = 0; j < MATRIX_SIZE; j++)
                {
                    word += matrix.ElementAt(j)[i];
                }
                list.Add(word);
            }
            return list;
        }

        public IEnumerable<string> Find(IEnumerable<string> wordstream)
        {
            try
            {
                var counters = wordstream.GroupBy(word => word).ToDictionary(group => group.Key, group => group.Count());
                var tasks = new List<Task>();

                foreach (var word in counters.Keys.ToList())
                {
                    tasks.Add(Task.Run(() =>
                    {
                        if (!(WordExistInMatrix(_horizontalMatrix, word) || WordExistInMatrix(_verticalMatrix, word)))
                        {
                            counters.Remove(word);
                        }
                    }));

                }
                Task.WaitAll(tasks.ToArray());

                return counters.OrderByDescending(counter => counter.Value).Take(10).Select(counter => counter.Key);
            }
            catch (Exception e)
            {
                throw new Exception("Execution Exception", e);
            }
        }

        private bool WordExistInMatrix(IEnumerable<string> matrix, string word)
        {
            return matrix.Any(line => line.Contains(word));
        }

    }

}
