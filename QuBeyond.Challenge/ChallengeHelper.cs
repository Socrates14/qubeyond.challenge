﻿using System.Collections.Generic;

namespace QuBeyond.Challenge
{
    public static class ChallengeHelper
    {
        public static IEnumerable<string> GetMatrix()
        {
            var matrix = new List<string>();
            //glass

            matrix.Add("winterasdasdasdasdasdasdasdecountryadasdsadasdddddddddddddwinter");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweaodqwec");
            matrix.Add("nosuncvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsurshini");
            matrix.Add("tlucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvdcbxxt");
            matrix.Add("ednlsundbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxy");
            matrix.Add("rasorasorasorasorasorasorasorasorasorasorasorasorasorasorasorasl");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxy");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxt");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesui");
            matrix.Add("winterasdasdasdasdasdasdasdeeasdasdadasdsadasdddddddddddddwintec");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweasdqwee");
            matrix.Add("nosuncvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsuncbxxx");
            matrix.Add("tlucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("ednlsundbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("rasorasorasorasorasorasorasorasorasorasorasorasorasorasorasorass");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxx");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxx");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("winterasdasdasdasdasdasdasdeeasdasdadasdsadasdddddddddddddwinter");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweasdqwee");
            matrix.Add("nosuncvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsuncbxxx");
            matrix.Add("tlucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("ednlsundbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("rasorasorasorasorasorasorasorasorasorasorasorasorasorasorasorass");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxx");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxx");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("winterasdasdasdasdasdasdasdeeasdasdadasdsadasdddddddddddddwinter");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweasdqwee");
            matrix.Add("nevercvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsuncbxxs");
            matrix.Add("tlucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxt");
            matrix.Add("ednlsundbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxo");
            matrix.Add("rasorasorasorasorasorasorasorasorasorasorasorasorasorasorasorasr");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxm");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxn");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("winterasdasdasdasdasdasdasdeeasdasdadasdsadasdddddddddddddwinter");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweasdqwee");
            matrix.Add("nosuncvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsuncbxxx");
            matrix.Add("tlucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxtable");
            matrix.Add("ednlsundbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("rasorasorasorasorasorasorasorasorasorasorasorasorasorasorastable");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxx");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxx");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("winterasdasdasdasdasdasdasdeeasdasdadasdsadasdddddddddddddwinter");
            matrix.Add("icoldqweqweqweqweqweqweqweddqweqweqeqweasdasdasdqweasdqweasdqwee");
            matrix.Add("nosuncvvbcvbcvbcvbcvbcvbsunvcbcvsunbcvbcvbcvbcvcbxxvcbcbsuncbxxx");
            matrix.Add("clucloudbcvbcvbcvbcvbcvbcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("adnlsundbcvbcvbcvbcvbcatcvbvcbcvbcvbcvbcvbcvbcvcbxxvcbcbxvxcbxxx");
            matrix.Add("tasorasorasorasorasorasorasorasorasorasorasorasorasorasorasorass");
            matrix.Add("zxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxcuzxxx");
            matrix.Add("zxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxcdzxxx");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");
            matrix.Add("stonesunqweqweqweqweqweqwerrqweqweqweqweqweqweqweqwewewestonesun");

            return matrix;
        }
        public static IEnumerable<string> GetWordStream()
        {
            return new List<string>
            { "sun", "winter", "cold", "cloud", "stone", "sunshine", "never", "storm", "chair", "table", "cat", "word", "price", "sun", "country", "city",
                "sun", "winter", "cold", "cloud", "stone", "sunshine", "never", "storm", "chair", "table", "cat", "word", "price", "sun", "country", "city",
                "sun", "winter", "cold", "cloud", "stone", "sunshine", "never", "storm", "chair", "table", "cat", "word", "price", "sun",
                "sun", "winter", "cold", "cloud", "stone", "sunshine", "never", "storm", "chair", "table", "cat", "word", "price", "sun",
                "kinetic","ladd","lexicographer","lorgnon","glass","machinated","nonassonant","nonanachronistic","marykay","mastodon","melanoma","multiplated","nonsegregative","ostrogothic","outspinned","oxygen","pagne","perikeiromene","phallic","photovolto","pilate","archaicism","aspectant","blazingly","babar","bertina","boxwood","bolometrically","brighouse","brimming","buttonlike","citral","tweenies","theoretics","trawlability","tribrach","undetestable","uninvertible","unrifted","wheelsman","zambia","refulgency","rippingness","polished","preinherit","pretheological","proctologic","propylhexedrine","quibble","raeburn","comanchean","cyprian","damar","decipherable","decolorizing","dumbly","ergodic","sanctuaries","egos","etherialising","semiexclusive","siderography","skerrick","subcrystalline","slask","snoring","solubly","squeamish","stripling","superendorse","fittipaldi","fides","forechoir","fulmination","gipsyism","indrani","hydracid","hypostatic","interfibrillary","interstadial","intransitiveness","kinetic","ladd","lexicographer","lorgnon","glass","machinated","nonassonant","nonanachronistic","marykay","mastodon","melanoma","multiplated","nonsegregative","ostrogothic","outspinned","oxygen","pagne","perikeiromene","phallic","photovolto","pilate","archaicism","aspectant","blazingly","babar","bertina","boxwood","bolometrically","brighouse","brimming","buttonlike","citral","tweenies","theoretics","trawlability","tribrach","undetestable","uninvertible","unrifted","wheelsman","zambia","refulgency","rippingness","polished","preinherit","pretheological","proctologic","propylhexedrine","quibble","raeburn","comanchean","cyprian","damar","decipherable","decolorizing","dumbly","ergodic","sanctuaries","egos","etherialising","semiexclusive","siderography","skerrick","subcrystalline","slask","snoring","solubly","squeamish","stripling","superendorse","fittipaldi","fides","forechoir","fulmination","gipsyism","indrani","hydracid","hypostatic","interfibrillary","interstadial","intransitiveness",
                "kinetic","ladd","lexicographer","lorgnon","glass","machinated","nonassonant","nonanachronistic","marykay","mastodon","melanoma","multiplated","nonsegregative","ostrogothic","outspinned","oxygen","pagne","perikeiromene","phallic","photovolto","pilate","archaicism","aspectant","blazingly","babar","bertina","boxwood","bolometrically","brighouse","brimming","buttonlike","citral","tweenies","theoretics","trawlability","tribrach","undetestable","uninvertible","unrifted","wheelsman","zambia","refulgency","rippingness","polished","preinherit","pretheological","proctologic","propylhexedrine","quibble","raeburn","comanchean","cyprian","damar","decipherable","decolorizing","dumbly","ergodic","sanctuaries","egos","etherialising","semiexclusive","siderography","skerrick","subcrystalline","slask","snoring","solubly","squeamish","stripling","superendorse","fittipaldi","fides","forechoir","fulmination","gipsyism","indrani","hydracid","hypostatic","interfibrillary","interstadial","intransitiveness","kinetic","ladd","lexicographer","lorgnon","glass","machinated","nonassonant","nonanachronistic","marykay","mastodon","melanoma","multiplated","nonsegregative","ostrogothic","outspinned","oxygen","pagne","perikeiromene","phallic","photovolto","pilate","archaicism","aspectant","blazingly","babar","bertina","boxwood","bolometrically","brighouse","brimming","buttonlike","citral","tweenies","theoretics","trawlability","tribrach","undetestable","uninvertible","unrifted","wheelsman","zambia","refulgency","rippingness","polished","preinherit","pretheological","proctologic","propylhexedrine","quibble","raeburn","comanchean","cyprian","damar","decipherable","decolorizing","dumbly","ergodic","sanctuaries","egos","etherialising","semiexclusive","siderography","skerrick","subcrystalline","slask","snoring","solubly","squeamish","stripling","superendorse","fittipaldi","fides","forechoir","fulmination","gipsyism","indrani","hydracid","hypostatic","interfibrillary","interstadial","intransitiveness",
             };
        }
    }
}
